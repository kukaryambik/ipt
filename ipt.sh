#!/usr/bin/env bash

### Set variables ###

WAN_IF=""
LAN_IF="lo"
TTL=""
NEW_RULES=""

PATH=/usr/local/sbin:/usr/sbin:/sbin:$PATH
IPT=iptables
IPTS=iptables-save
IPTR=iptables-restore
NOW_DATE="$(date '+%Y%m%d_%H%M%S')"
OLD_RULES="`dirname $0`/iptables_${NOW_DATE}.backup"
if ! [ "$WAN_IF" ]; then
  WAN_IF="$(ip link \
           | awk ' $1 ~ /[0-9+]/ {print $2}' \
           | sed -e "s@\(lo\|$(echo $LAN_IF | sed 's@[[:space:]]@\\\|@g')\|\:\)@@g")"
fi
LAN_IP="$(for i in $LAN_IF; do ip r | awk '$3 ~ /'$i'/ {print $1}'; done)"
if ! [ "$NEW_RULES" ]; then
  NEW_RULES="`dirname $0`/iptables.rules"
fi

### Clear the rules ###
clr_rls() {
echo -e "\nClear the rules"
$IPT -F
$IPT -F -t nat
$IPT -F -t mangle
$IPT -X
$IPT -t nat -X
$IPT -t mangle -X
$IPT -P OUTPUT ACCEPT
$IPT -P INPUT ACCEPT
$IPT -P FORWARD ACCEPT
}

### Default rules ###
default() {

echo === Deny all by default ===
$IPT -P INPUT DROP
$IPT -P FORWARD DROP

echo === Allow local ===
for VAR in $LAN_IF; do
  $IPT -t mangle -A PREROUTING -i $VAR -j ACCEPT
  $IPT -A INPUT -i $VAR -j ACCEPT
done

if grep -qP '(^(\d+\.){3}\d+)' `dirname $0`/whitelist.txt; then
  echo === Whitelist allow ===
  for VAR in $(grep -oP '(^(\d+\.){3}\d+(\/\d+|\/(\d+\.){3}\d+|))' `dirname $0`/whitelist.txt); do
    $IPT -t mangle -A PREROUTING -s $VAR -j ACCEPT
    $IPT -A INPUT -s $VAR -j ACCEPT
  done
fi

if grep -qP '(^(\d+\.){3}\d+)' `dirname $0`/blacklist.txt; then
  echo === Blacklist block ===
  for VAR in $(grep -oP '(^(\d+\.){3}\d+(\/\d+|\/(\d+\.){3}\d+|))' `dirname $0`/blacklist.txt); do
    for VAR2 in $LAN_IF; do
      $IPT -t mangle -A PREROUTING -s $VAR ! -i $VAR2 -j DROP
    done
  done
fi

echo === Allow establish and related ===
$IPT -t mangle -A PREROUTING -m state --state ESTABLISHED,RELATED -j ACCEPT
$IPT -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT

echo === Mangle table ===

### Drop fragments in all chains ###
$IPT -t mangle -A PREROUTING -f -j DROP

### Drop TCP packets that are new and are not SYN ###
$IPT -t mangle -A PREROUTING -p tcp ! --syn -m state --state NEW -j DROP

### Drop SYN packets with suspicious MSS value ###
$IPT -t mangle -A PREROUTING -p tcp -m state --state NEW --syn -m tcpmss ! --mss 536:65535 -j DROP

### Nmap block ###
if ! [ $(find `dirname $0`/ -type f -name 'pf.os') ]; then
  curl -s http://bxr.su/raw/OpenBSD/etc/pf.os -o `dirname $0`/pf.os
fi
nfnl_osf -f `dirname $0`/pf.os > /dev/null 2>&1
$IPT -t mangle -A PREROUTING -p tcp -m osf --genre nmap -j DROP

### Drop invalid packets ###
$IPT -t mangle -A PREROUTING -m state --state INVALID -j DROP

### Block packets with bogus TCP flags ###
$IPT -t mangle -N FLAG_BLCK
$IPT -t mangle -A FLAG_BLCK -p tcp --tcp-flags SYN,ACK,FIN,RST RST -m limit --limit 3/s --limit-burst 2 -j RETURN
$IPT -t mangle -A FLAG_BLCK -p tcp --tcp-flags RST RST -m limit --limit 3/s --limit-burst 2 -j RETURN
$IPT -t mangle -A FLAG_BLCK -p tcp -m limit --limit 1/m --limit-burst 1 -j LOG --log-prefix "IPT: BAD FLAGS: " --log-level info
$IPT -t mangle -A FLAG_BLCK -j DROP

$IPT -t mangle -A PREROUTING -p tcp --tcp-flags RST RST -j FLAG_BLCK
$IPT -t mangle -A PREROUTING -p tcp --tcp-flags SYN,ACK,FIN,RST RST -j FLAG_BLCK
$IPT -t mangle -A PREROUTING -p tcp --tcp-flags ALL FIN -j FLAG_BLCK
$IPT -t mangle -A PREROUTING -p tcp --tcp-flags ALL SYN,FIN -j FLAG_BLCK
$IPT -t mangle -A PREROUTING -p tcp --tcp-flags ALL URG,PSH,FIN -j FLAG_BLCK
$IPT -t mangle -A PREROUTING -p tcp --tcp-flags ACK,FIN FIN -j FLAG_BLCK
$IPT -t mangle -A PREROUTING -p tcp --tcp-flags ACK,PSH PSH -j FLAG_BLCK
$IPT -t mangle -A PREROUTING -p tcp --tcp-flags ACK,URG URG -j FLAG_BLCK
$IPT -t mangle -A PREROUTING -p tcp --tcp-flags ALL ALL -j FLAG_BLCK
$IPT -t mangle -A PREROUTING -p tcp --tcp-flags ALL NONE -j FLAG_BLCK
$IPT -t mangle -A PREROUTING -p tcp --tcp-flags ALL URG,PSH,SYN,FIN -j FLAG_BLCK

echo === Filter table ===

### Limit connections per source IP ### 
$IPT -A INPUT -p tcp -m connlimit --connlimit-above 200 -j REJECT --reject-with tcp-reset

### Block SYN flood ###
$IPT -N SYN_FLD
$IPT -A SYN_FLD -m limit --limit 4/s --limit-burst 2 -j RETURN
$IPT -A SYN_FLD -j DROP
$IPT -A INPUT -p tcp --syn -j SYN_FLD

### Limit new TCP connections per second per source IP ###
$IPT -N CON_LIM
$IPT -A CON_LIM -m limit --limit 90/s --limit-burst 60 -j RETURN
$IPT -A CON_LIM -j DROP
$IPT -A INPUT -p tcp -m state --state NEW -j CON_LIM

### Access filtering chain ###
$IPT -N ACCESS_CHAIN
$IPT -A ACCESS_CHAIN -m recent --name ACCESS --update --seconds 60 --hitcount 3 -j DROP
$IPT -A ACCESS_CHAIN -m recent --name ACCESS --set -j ACCEPT
$IPT -A ACCESS_CHAIN -j LOG --log-prefix "IPT: ACCESS-WARNING: " --log-level warning

echo === Allow SSH ===
SSH_PORT=`find /etc/ -iname sshd_config -exec awk '$1 == "Port" {print $2}' {} \;`
if ! [ "$SSH_PORT" ]; then SSH_PORT="22"; fi
$IPT -A INPUT -p tcp -m multiport --dport $SSH_PORT -m state --state NEW -j ACCESS_CHAIN

if [ "$TTL" ]; then
  echo === Set TTL ===
  for i in $WAN_IF; do
    $IPT -t mangle -A POSTROUTING -o $i -j TTL --ttl-set $TTL
  done
fi

}

oports() {
LN=`$IPT -t mangle -L --line-numbers | awk '/INVALID/{print $1}'`
let LN=$LN+1

if ! [ "$ALLOW_IP" ]; then
  ALLOW_IP="0/0"
fi

for IP in $ALLOW_IP; do
  if [ "$TCP_PORTS" ]; then
    for VAR in $TCP_PORTS; do
      $IPT -t mangle -A PREROUTING $LN -p tcp -s $IP -m multiport --dport $VAR -j ACCEPT
      $IPT -A INPUT -p tcp -s $IP -m multiport --dport $VAR -j ACCEPT
    done
  fi

  if [ "$UDP_PORTS" ]; then
    for VAR in $UDP_PORTS; do
      $IPT -t mangle -A PREROUTING $LN -p udp -s $IP -m multiport --dport $VAR -j ACCEPT
      $IPT -A INPUT -p udp -s $IP -m multiport --dport $VAR -j ACCEPT
    done
  fi
done

ALLOW_IP=""
TCP_PORTS=""
UDP_PORTS=""
}

cleanit() {
echo -e "\nSaving old rules in $OLD_RULES"
$IPTS -c > $OLD_RULES

clr_rls

echo -e "\nSaving clean rules in $NEW_RULES"
$IPTS -c > $NEW_RULES
echo -e "\nDone"

exit 0
}

init() {
echo -e "\nSaving old rules in $OLD_RULES"
$IPTS -c > $OLD_RULES

clr_rls

echo -e "\nInitiate default rules:"
default
echo -e "\nInitiate additional rules:"
for CONF in $(find `dirname $0`/conf.d/ -follow -type f -iname '*.conf'); do
. $CONF
done

echo -e "\nSaving new rules in $NEW_RULES"
$IPTS -c > $NEW_RULES
echo -e "\nDone"

exit 0
}

# Start with arguments
echo $@ | egrep -q -- '-nobackup( |$)' && export OLD_RULES="/dev/null"
echo $@ | egrep -q -- '-(c|clear)( |$)' && cleanit
echo $@ | egrep -q -- '-(i|init)( |$)' && init

# Variables
echo -e "\nWAN interfaces:"
echo $WAN_IF | tr '\n' ' '
echo -e "\nLAN interfaces:\n$LAN_IF"
echo -e "LAN addresses:"
echo $LAN_IP | tr '\n' ' '
echo -e "\n"

PS3='Select an action: '
options=("Clear the rules"\
	 "Initiate IPTables"\
	 "Quit")
select opt in "${options[@]}"; do
  case $opt in
    "Clear the rules") cleanit;;
    "Initiate IPTables") init;;
    "Quit") break;;
    *) break;;
  esac
done

exit 0

